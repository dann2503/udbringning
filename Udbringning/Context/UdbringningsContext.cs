﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Udbringning.Entities;

namespace Udbringning.Context
{
    public class UdbringningsContext : DbContext
    {
        public UdbringningsContext(DbContextOptions<UdbringningsContext> options)
            : base(options) { }

        public DbSet<UserEntity> Users { get; set; }
    }
}
