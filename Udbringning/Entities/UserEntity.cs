﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Udbringning.Entities
{
    public class UserEntity
    {
        public int Id { get; set; }

        public string firstName { get; set; }

        public string lastName { get; set; }

        public string email { get; set; }

        public int phoneNumber { get; set; }

        public string address { get; set; }
        
        public int zipCode { get; set; }

    }
}
